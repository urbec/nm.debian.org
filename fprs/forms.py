from django import forms


class EndorsementForm(forms.Form):
    text = forms.CharField(
        label="Signed statement", widget=forms.Textarea(attrs={"rows": 25, "cols": 80, "class": "text-monospace"}))

    def __init__(self, *args, **kw):
        self.endorsing_fpr = kw.pop("endorsing_fpr")
        super(EndorsementForm, self).__init__(*args, **kw)

    def clean_text(self):
        from keyring.models import Key
        text = self.cleaned_data["text"]

        try:
            key = Key.objects.get_or_download(self.endorsing_fpr)
        except RuntimeError as e:
            raise forms.ValidationError("Cannot download the key: " + str(e))

        try:
            plaintext = key.verify(text)
        except RuntimeError as e:
            raise forms.ValidationError(
                "Cannot verify the signature: " + str(e))

        return (text, plaintext)


class EndorsementDeletionForm(forms.Form):
    delete = forms.TypedChoiceField(
        coerce=lambda x: x =='True',
        choices=(
            (False, 'No'),
            (True, 'Yes'),
        )
    )

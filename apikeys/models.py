from django.db import models
from django.conf import settings

# This is only used by:
#  - DebConf (queries /api/people?email=*)
#  - contributors.debian.org (queries /api/status?status=dd_u,dd_nu in ContributorsMIA)


class Key(models.Model):
    """
    An API access token and its associated user
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="apikeys", on_delete=models.CASCADE)
    name = models.CharField(max_length=16)
    value = models.CharField(max_length=16, unique=True)
    enabled = models.BooleanField(default=True)


class AuditLog(models.Model):
    """
    Audit log for token usage
    """
    key = models.ForeignKey(Key, related_name="audit_log", on_delete=models.CASCADE)
    ts = models.DateTimeField(auto_now_add=True)
    key_enabled = models.BooleanField(default=False)
    remote_addr = models.CharField(max_length=255)
    request_method = models.CharField(max_length=8)
    absolute_uri = models.TextField()

from __future__ import annotations
from django.urls import path
from . import views

app_name = "apikeys"

urlpatterns = [
    path('', views.KeyList.as_view(), name="list"),
    path('<int:pk>/enable/', views.KeyEnable.as_view(), name="enable"),
    path('<int:pk>/delete/', views.KeyDelete.as_view(), name="delete"),
]

from django.test import TestCase
from django.utils.timezone import now
from backend import const
from backend.unittest import PageElements
import process.models as pmodels
from unittest.mock import patch
from process.unittest import ProcessFixtureMixin


class ProcessShowTestMixin(ProcessFixtureMixin):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.visitor = cls.persons.dc

        cls.setUpProcess()
        cls.processes.app.add_log(cls.persons.fd, "xxxx_public_xxxx", is_public=True)
        cls.processes.app.add_log(cls.persons.fd, "xxxx_private_xxxx", is_public=False)
        cls.processes.app.add_log(cls.visitor, "xxxx_ownprivate_xxxx", is_public=False)

        cls.page_elements = PageElements()
        cls.page_elements.add_id("edit_fpr_link")
        cls.page_elements.add_id("view_person_fd_comment")
        cls.page_elements.add_id("view_process_fd_comment")
        cls.page_elements.add_id("view_mbox")
        cls.page_elements.add_id("log_form")
        cls.page_elements.add_id("log_public")
        cls.page_elements.add_id("log_private")
        cls.page_elements.add_id("proc_freeze")
        cls.page_elements.add_id("proc_unfreeze")
        cls.page_elements.add_id("proc_approve")
        cls.page_elements.add_id("proc_unapprove")
        cls.page_elements.add_id("download_statements")
        cls.page_elements.add_id("req_intent_missing")
        cls.page_elements.add_id("req_intent_ok")
        cls.page_elements.add_id("req_sc_dmup_missing")
        cls.page_elements.add_id("req_sc_dmup_ok")
        cls.page_elements.add_id("req_advocate_missing")
        cls.page_elements.add_id("req_advocate_ok")
        cls.page_elements.add_id("req_keycheck_missing")
        cls.page_elements.add_id("req_keycheck_ok")
        cls.page_elements.add_id("req_am_ok_missing")
        cls.page_elements.add_id("req_am_ok_ok")
        cls.page_elements.add_string("view_public_log", "xxxx_public_xxxx")
        cls.page_elements.add_string("view_private_log", "xxxx_private_xxxx")
        cls.page_elements.add_string("view_ownprivate_log", "xxxx_ownprivate_xxxx")

    @classmethod
    def setUpProcess(cls):
        pass

    def tryVisitingWithPerms(self, perms, elements):
        def requirement_perms(req, visitor):
            return set(perms[req.type])

        client = self.make_test_client(self.visitor)
        with patch.object(pmodels.Process, "permissions_of", return_value=set(perms["process"])):
            with patch.object(pmodels.Requirement, "permissions_of", requirement_perms):
                response = client.get(self.processes.app.get_absolute_url())
                self.assertEqual(response.status_code, 200)
                self.assertContainsElements(response, self.page_elements, *elements)
        return response

    def perms(self, **kw):
        kw.setdefault("process", ())
        kw.setdefault("intent", ("req_view",))
        kw.setdefault("sc_dmup", ("req_view",))
        kw.setdefault("advocate", ("req_view",))
        kw.setdefault("keycheck", ("req_view",))
        kw.setdefault("am_ok", ("req_view",))
        return kw


class TestDDProcessShow(ProcessShowTestMixin, TestCase):
    @classmethod
    def setUpProcess(cls):
        super().setUpProcess()
        cls.create_person("app", status=const.STATUS_DC, fd_comment="test")
        cls.processes.create("app", person=cls.persons.app, applying_for=const.STATUS_DD_U, fd_comment="test")
        cls.create_person("am", status=const.STATUS_DD_NU)
        cls.ams.create("am", person=cls.persons.am)
        cls.amassignments.create(
                "am", process=cls.processes.app, am=cls.ams.am, assigned_by=cls.persons["fd"], assigned_time=now())

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.common_elements = (
                "view_public_log", "view_ownprivate_log",
                "download_statements",
                "req_intent_missing", "req_sc_dmup_missing",
                "req_advocate_missing", "req_keycheck_missing",
                "req_am_ok_missing")

    def test_none(self):
        self.tryVisitingWithPerms(self.perms(), self.common_elements)

    def test_edit_ldap(self):
        self.tryVisitingWithPerms(
            self.perms(process=("edit_ldap", "edit_fpr")),
            self.common_elements + ("edit_fpr_link",),
        )

    def test_view_private_log(self):
        self.tryVisitingWithPerms(
            self.perms(process=("view_private_log",)),
            self.common_elements + ("view_private_log",),
        )

    def test_view_mbox(self):
        self.tryVisitingWithPerms(
            self.perms(process=("view_mbox",)),
            self.common_elements + ("view_mbox",),
        )

    def test_fd_comments(self):
        self.tryVisitingWithPerms(
            self.perms(process=("fd_comments",)),
            self.common_elements + ("view_person_fd_comment", "view_process_fd_comment"),
        )

    def test_add_log(self):
        self.tryVisitingWithPerms(
            self.perms(process=("add_log",)),
            self.common_elements + ("log_public", "log_private", "log_form"),
        )

    def test_proc_freeze(self):
        self.tryVisitingWithPerms(
            self.perms(process=("add_log", "proc_freeze")),
            self.common_elements + ("log_public", "log_private", "log_form", "proc_freeze"),
        )

    def test_proc_unfreeze(self):
        self.tryVisitingWithPerms(
            self.perms(process=("add_log", "proc_unfreeze")),
            self.common_elements + ("log_public", "log_private", "log_form", "proc_unfreeze"),
        )

    def test_proc_approve(self):
        self.tryVisitingWithPerms(
            self.perms(process=("add_log", "proc_approve")),
            self.common_elements + ("log_public", "log_private", "log_form", "proc_approve"),
        )

    def test_proc_unapprove(self):
        self.tryVisitingWithPerms(
            self.perms(process=("add_log", "proc_unapprove")),
            self.common_elements + ("log_public", "log_private", "log_form", "proc_unapprove"),
        )


class TestDEProcessShow(ProcessShowTestMixin, TestCase):
    @classmethod
    def setUpProcess(cls):
        super().setUpProcess()
        cls.create_person("app", status=const.STATUS_DD_U, fd_comment="test")
        cls.processes.create("app", person=cls.persons.app, applying_for=const.STATUS_EMERITUS_DD, fd_comment="test")

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.common_elements = ("view_public_log", "view_ownprivate_log", "download_statements")

    def perms(self, **kw):
        kw.setdefault("process", ())
        return kw

    def test_none(self):
        self.tryVisitingWithPerms(self.perms(process=()), self.common_elements)

    def test_view_intent(self):
        self.tryVisitingWithPerms(
            self.perms(intent=("req_view",)),
            self.common_elements + ("req_intent_missing",),
        )

    def test_edit_ldap(self):
        self.tryVisitingWithPerms(
            self.perms(process=("edit_ldap", "edit_fpr")),
            self.common_elements + ("edit_fpr_link",),
        )

    def test_view_private_log(self):
        self.tryVisitingWithPerms(
            self.perms(process=("view_private_log",)),
            self.common_elements + ("view_private_log",),
        )

    def test_view_mbox(self):
        self.tryVisitingWithPerms(
            self.perms(process=("view_mbox",)),
            self.common_elements + ("view_mbox",),
        )

    def test_fd_comments(self):
        self.tryVisitingWithPerms(
            self.perms(process=("fd_comments",)),
            self.common_elements + ("view_person_fd_comment", "view_process_fd_comment"),
        )

    def test_add_log(self):
        self.tryVisitingWithPerms(
            self.perms(process=("add_log",)),
            self.common_elements + ("log_public", "log_private", "log_form"),
        )

    def test_proc_freeze(self):
        self.tryVisitingWithPerms(
            self.perms(process=("add_log", "proc_freeze")),
            self.common_elements + ("log_public", "log_private", "log_form", "proc_freeze"),
        )

    def test_proc_unfreeze(self):
        self.tryVisitingWithPerms(
            self.perms(process=("add_log", "proc_unfreeze")),
            self.common_elements + ("log_public", "log_private", "log_form", "proc_unfreeze"),
        )

    def test_proc_approve(self):
        self.tryVisitingWithPerms(
            self.perms(process=("add_log", "proc_approve")),
            self.common_elements + ("log_public", "log_private", "log_form", "proc_approve"),
        )

    def test_proc_unapprove(self):
        self.tryVisitingWithPerms(
            self.perms(process=("add_log", "proc_unapprove")),
            self.common_elements + ("log_public", "log_private", "log_form", "proc_unapprove"),
        )

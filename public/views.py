from django import http, forms
from django.shortcuts import redirect
from django.urls import reverse
from django.core.exceptions import PermissionDenied
from django.utils.translation import gettext as _
from django.utils.timezone import now
from django.views.generic import View
from django.views.generic.edit import FormView
from django.contrib import messages as django_messages
import logging
import backend.models as bmodels
import backend.email as bemail
import dsa.models as dmodels
import legacy.models as lmodels
import process.models as pmodels
import nm2.lib.forms
from nm2.lib.multiform import Multiform
from nm2.lib.forms import BootstrapAttrsMixin
from nm2.lib import assets
from backend import const
from backend.mixins import VisitorMixin, VisitorTemplateView
from collections import Counter
import datetime
import json

log = logging.getLogger("django.request")


def lookup_or_404(dict, key):
    """
    Lookup a key in a dictionary, raising 404 if not found
    """
    try:
        return dict[key]
    except KeyError:
        raise http.Http404


class Managers(VisitorTemplateView):
    assets = [assets.DataTablesBootstrap4]
    template_name = "public/managers.html"

    def get_context_data(self, **kw):
        ctx = super(Managers, self).get_context_data(**kw)
        from django.db import connection

        # Compute statistics indexed by AM id
        with connection.cursor() as cursor:
            cursor.execute("""
            SELECT am.id,
                   count(*) as total,
                   sum(case when legacy_process.is_active then 1 else 0 end) as active,
                   sum(case when legacy_process.progress=%s then 1 else 0 end) as held
              FROM am
              JOIN legacy_process ON legacy_process.manager_id=am.id
             GROUP BY am.id
            """, (const.PROGRESS_AM_HOLD,))
            stats = {}
            for amid, total, active, held in cursor:
                stats[amid] = (total, active, held)

            cursor.execute("""
            SELECT am.id,
                   count(*) as total,
                   sum(case when p.closed_time is null then 1 else 0 end) as active,
                   sum(case when pam.paused then 1 else 0 end) as held
              FROM am
              JOIN process_amassignment pam ON pam.am_id=am.id
              JOIN process_process p ON pam.process_id=p.id
             GROUP BY am.id
            """)
            for amid, total, active, held in cursor:
                s = stats.get(amid, (0, 0, 0))
                stats[amid] = (s[0] + total, s[1] + active, s[2] + held)

        # Read the list of AMs, with default sorting, and annotate with the
        # statistics
        ams = []
        for a in bmodels.AM.objects.all().order_by("-is_am", "person__uid"):
            total, active, held = stats.get(a.id, (0, 0, 0))
            a.stats_total = total
            a.stats_active = active
            a.stats_done = total-active
            a.stats_held = held
            ams.append(a)

        ctx["ams"] = ams
        return ctx


SIMPLIFY_STATUS = {
    const.STATUS_DC: "new",
    const.STATUS_DC_GA: "new",
    const.STATUS_DM: "dm",
    const.STATUS_DM_GA: "dm",
    const.STATUS_DD_U: "dd",
    const.STATUS_DD_NU: "dd",
    const.STATUS_EMERITUS_DD: "emeritus",
    const.STATUS_REMOVED_DD: "removed",
}


class People(VisitorTemplateView):
    assets = [assets.DataTablesBootstrap4]
    template_name = "public/people.html"

    def get_context_data(self, **kw):
        ctx = super(People, self).get_context_data(**kw)
        status = self.kwargs.get("status", None)

        # def people(request, status=None):
        objects = bmodels.Person.objects.all().order_by("ldap_fields__uid", "fullname")
        show_status = True
        status_sdesc = None
        status_ldesc = None
        if status:
            if status == "dm_all":
                objects = objects.filter(status__in=(const.STATUS_DM, const.STATUS_DM_GA))
                status_sdesc = _("Debian Maintainer")
                status_ldesc = _("Debian Maintainer (with or without guest account)")
            elif status == "dd_all":
                objects = objects.filter(status__in=(const.STATUS_DD_U, const.STATUS_DD_NU))
                status_sdesc = _("Debian Developer")
                status_ldesc = _("Debian Developer (uploading or not)")
            else:
                objects = objects.filter(status=status)
                show_status = False
                status_sdesc = lookup_or_404(const.ALL_STATUS_BYTAG, status).sdesc
                status_ldesc = lookup_or_404(const.ALL_STATUS_BYTAG, status).sdesc

        people = []
        for p in objects:
            p.simple_status = SIMPLIFY_STATUS.get(p.status, None)
            people.append(p)

        ctx.update(
            people=people,
            status=status,
            show_status=show_status,
            status_sdesc=status_sdesc,
            status_ldesc=status_ldesc,
        )
        return ctx


class Members(VisitorTemplateView):
    assets = [assets.DataTablesBootstrap4]
    template_name = "public/members.html"

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        ctx["members"] = bmodels.Person.objects.select_related("ldap_fields").filter(
                status__in=(const.STATUS_DD_U, const.STATUS_DD_NU)).order_by("fullname")
        return ctx


class AuditLog(VisitorTemplateView):
    template_name = "public/audit_log.html"
    require_visitor = "dd"

    def get_context_data(self, **kw):
        ctx = super(AuditLog, self).get_context_data(**kw)

        audit_log = []
        cutoff = now() - datetime.timedelta(days=30)
        for e in bmodels.PersonAuditLog.objects.filter(logdate__gte=cutoff).order_by("-logdate"):
            if self.request.user.is_superuser:
                changes = sorted((k, v[0], v[1]) for k, v in list(json.loads(e.changes).items()))
            else:
                changes = sorted((k, v[0], v[1]) for k, v in list(json.loads(e.changes).items()) if k != "fd_comment")
            audit_log.append({
                "person": e.person,
                "logdate": e.logdate,
                "author": e.author,
                "notes": e.notes,
                "changes": changes,
            })

        ctx["audit_log"] = audit_log
        return ctx


class Stats(VisitorTemplateView):
    assets = [assets.DataTablesBootstrap4, assets.Flot]
    template_name = "public/stats.html"

    def get_context_data(self, **kw):
        ctx = super(Stats, self).get_context_data(**kw)
        from django.db.models import Count

        stats = {}

        # Count of people by status
        by_status = []
        for row in bmodels.Person.objects.values("status").annotate(count=Count("status")).order_by("status"):
            by_status.append((row["status"], row["count"]))
        by_status.sort(key=lambda x: -x[1])
        stats["by_status"] = by_status

        # Cook up more useful bits for the templates

        ctx["stats"] = stats

        # List of active processes with statistics
        active_processes = []

        # Build process list
        import process.models as pmodels
        from process.mixins import compute_process_status
        for p in pmodels.Process.objects.filter(closed_time__isnull=True):
            active_processes.append((p, compute_process_status(p, self.request.user)))
        active_processes.sort(key=lambda x: (x[1]["log_first"].logdate if x[1]["log_first"] else x[0].started))
        ctx["active_processes"] = active_processes

        # Count processes by status
        by_status = Counter()
        for proc, stat in active_processes:
            by_status[stat["summary"]] += 1
        ctx["by_status"] = sorted(by_status.items())

        # Count of applicants by progress (for rrdstats.sh, merge old and new processes)
        by_progress = Counter()
        by_progress["dam_ok"] += by_status["Approved"]
        by_progress["am"] += by_status["AM"]
        by_progress["am_hold"] += by_status["AM Hold"]
        by_progress["am_ok"] += by_status["Waiting for review"]
        by_progress["fd_ok"] += by_status["Frozen for review"]
        by_progress["app_new"] += by_status["Collecting requirements"]
        stats["by_progress"] = by_progress

        return ctx

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        # If JSON is requested, dump them right away
        if 'json' in request.GET:
            res = http.HttpResponse(content_type="application/json")
            res["Content-Disposition"] = "attachment; filename=stats.json"
            json.dump(context["stats"], res, indent=1)
            return res
        else:
            return self.render_to_response(context)


def make_findperson_form(request, visitor):
    includes = ["status", "email"]

    if visitor.is_superuser:
        includes.append("fd_comment")

    class FindpersonForm(nm2.lib.forms.BootstrapAttrsMixin, forms.ModelForm):
        name = forms.CharField(
                label="Name", required=False, min_length=40, widget=forms.TextInput(attrs={"autofocus": "autofocus"}))
        uid = forms.CharField(
                label="Uid", required=False, min_length=40)
        fpr = forms.CharField(
                label="Fingerprint", required=False, min_length=40, widget=forms.TextInput(attrs={"size": 60}))

        class Meta:
            model = bmodels.Person
            fields = includes

        def clean_fpr(self):
            return bmodels.FingerprintField.clean_fingerprint(self.cleaned_data['fpr'])

        def __init__(self, *args, **kw):
            super().__init__(*args, **kw)
            f = self.fields.get("fd_comment")
            if f is not None:
                f.widget = forms.TextInput(attrs={"class": "form-control"})

            # Reorder fields putting name at the beginning
            sorted_fields = {
                "name": self.fields.pop("name"),
                "uid": self.fields.pop("uid"),
                "fpr": self.fields.pop("fpr"),
            }
            for k, v in self.fields.items():
                sorted_fields[k] = v
            self.fields = sorted_fields

    return FindpersonForm


class Findperson(VisitorTemplateView):
    assets = [assets.DataTablesBootstrap4]
    template_name = "public/findperson.html"

    def get_form_class(self):
        return make_findperson_form(self.request, self.request.user)

    def get_context_data(self, **kw):
        Form = self.get_form_class()
        return super().get_context_data(form=Form())


class StatsGraph(VisitorTemplateView):
    template_name = "public/stats_graph.html"

    def get_context_data(self, **kw):
        ctx = super(StatsGraph, self).get_context_data(**kw)

        am_nm = []
        for p in lmodels.Process.objects.filter(manager__isnull=False).values_list(
                "manager__person__ldap_fields__uid", "person__ldap_fields__uid"):
            am_nm.append((p[0], p[1]))

        for a in pmodels.AMAssignment.objects.filter(unassigned_time__isnull=True).values_list(
                "am__person__ldap_fields__uid", "process__person__ldap_fields__uid"):
            am_nm.append((a[0], a[1]))

        adv_nm = []
        for p in lmodels.Process.objects.all().select_related("person__ldap_fields"):
            for a in p.advocates.all().select_related("ldap_fields"):
                person_uid = p.person.ldap_fields.uid
                if not person_uid:
                    continue
                adv_nm.append((a.ldap_fields.uid, person_uid))

        for s in pmodels.Statement.objects.filter(requirement__type="advocate").values_list(
                "fpr__person__ldap_fields__uid", "requirement__process__person__ldap_fields__uid"):
            adv_nm.append((s[0], s[1]))

        ctx = dict(
            am_nm=am_nm,
            adv_nm=adv_nm,
        )

        return ctx


YESNO = (
    ("yes", "Yes"),
    ("no", "No"),
)


class NewPersonForm(BootstrapAttrsMixin, forms.ModelForm):
    fpr = forms.CharField(label="Fingerprint", min_length=40, widget=forms.TextInput(attrs={"size": 60}))
    sc_ok = forms.ChoiceField(choices=YESNO, widget=forms.RadioSelect(), label="SC and DFSG agreement")
    dmup_ok = forms.ChoiceField(choices=YESNO, widget=forms.RadioSelect(), label="DMUP agreement")

    def clean_fpr(self):
        data = bmodels.FingerprintField.clean_fingerprint(self.cleaned_data['fpr'])
        if bmodels.Fingerprint.objects.filter(fpr=data).exists():
            raise forms.ValidationError(
                    "The GPG fingerprint is already known to this system."
                    " Please contact Front Desk to link your Alioth account to it.")
        return data

    def clean_sc_ok(self):
        data = self.cleaned_data['sc_ok']
        if data != "yes":
            raise forms.ValidationError("You need to agree with the Debian Social Contract and DFSG to continue")
        return data

    def clean_dmup_ok(self):
        data = self.cleaned_data['dmup_ok']
        if data != "yes":
            raise forms.ValidationError("You need to agree with the DMUP to continue")
        return data

    class Meta:
        model = bmodels.Person
        fields = ["fullname", "email", "bio"]
        widgets = {
            "bio": forms.Textarea(attrs={'cols': 80, 'rows': 25}),
        }


class NewPersonLDAPFieldsForm(BootstrapAttrsMixin, forms.ModelForm):
    class Meta:
        model = dmodels.LDAPFields
        fields = ["cn", "mn", "sn", "uid"]


class NewPersonMultiform(Multiform):
    form_classes = {
        "person": NewPersonForm,
        "ldap_fields": NewPersonLDAPFieldsForm,
    }


class Newnm(VisitorMixin, FormView):
    """
    Display the new Person form
    """
    template_name = "public/newnm.html"
    form_class = NewPersonMultiform
    DAYS_VALID = 3

    def get_success_url(self):
        return redirect("public_newnm_resend_challenge", key=self.request.user.lookup_key)

    def get_initial(self):
        # Prefill person.fullname from currently active identities
        person = {}
        for identity in self.request.signon_identities.values():
            if identity.fullname:
                person["fullname"] = identity.fullname
                break

        ldap_fields = {}
        return {
            "person": person,
            "ldap_fields": ldap_fields,
        }

    def form_valid(self, form):
        if self.request.user.is_authenticated:
            raise PermissionDenied
        if not self.request.signon_identities:
            raise PermissionDenied

        person = form["person"].save(commit=False)
        person.status = const.STATUS_DC
        person.status_changed = now()
        person.make_pending(days_valid=self.DAYS_VALID)
        person.save(audit_author=person, audit_notes="new subscription to the site")

        ldap_fields = form["ldap_fields"].save(commit=False)
        ldap_fields.person = person
        ldap_fields.save(audit_author=person, audit_notes="new subscription to the site")

        for identity in self.request.signon_identities.values():
            identity.person = person
            identity.save(audit_author=person, audit_notes="new subscription to the site")

        fpr = form["person"].cleaned_data["fpr"]
        bmodels.Fingerprint.objects.create(
                person=person, fpr=fpr, is_active=True, audit_author=person, audit_notes="new subscription to the site")

        # Redirect to the send challenge page
        return redirect("public_newnm_resend_challenge", key=person.lookup_key)

    def get_context_data(self, **kw):
        ctx = super().get_context_data(**kw)
        form = ctx["form"]
        errors = []
        for k, v in form.forms["person"].errors.items():
            if k in ("sc_ok", "dmup_ok"):
                section = "rules"
            else:
                section = k
            errors.append({
                "section": section,
                "label": form.forms["person"].fields[k].label,
                "id": k,
                "errors": v,
            })
        for k, v in form.forms["ldap_fields"].errors.items():
            if k in ("cn", "mn", "sn"):
                section = "name"
            else:
                section = k
            errors.append({
                "section": section,
                "label": form.forms["ldap_fields"].fields[k].label,
                "id": k,
                "errors": v,
            })

        has_entry = self.request.user.is_authenticated
        is_dd = self.request.user.is_authenticated and "dd" in self.request.user.perms
        require_login = not self.request.signon_identities
        show_apply_form = not require_login and (not has_entry or is_dd)

        ctx.update(
            person=self.request.user,
            form=form,
            errors=errors,
            has_entry=has_entry,
            is_dd=is_dd,
            show_apply_form=show_apply_form,
            require_login=require_login,
            DAYS_VALID=self.DAYS_VALID,
            wikihelp="https://wiki.debian.org/nm.debian.org/Newnm",
        )
        return ctx


class NewnmResendChallenge(VisitorMixin, View):
    """
    Send/resend the encrypted email nonce for people who just requested a new
    Person record
    """
    def get(self, request, key=None, *args, **kw):
        from keyring.models import Key, GpgInvocationError

        if not self.request.user.is_authenticated:
            raise PermissionDenied()

        # Deal gracefully with someone clicking the reconfirm link after they have
        # already confirmed
        if not self.request.user.pending:
            return redirect(self.request.user.get_absolute_url())

        confirm_url = request.build_absolute_uri(
                reverse("public_newnm_confirm", kwargs=dict(nonce=self.request.user.pending)))
        plaintext = "Please visit {} to confirm your application at {}\n".format(
                confirm_url,
                request.build_absolute_uri(self.request.user.get_absolute_url()))
        key = Key.objects.get_or_download(self.request.user.fpr)
        if not key.key_is_fresh():
            key.update_key()
        try:
            encrypted = key.encrypt(plaintext.encode("utf8"))
        except GpgInvocationError:
            django_messages.error(request, _(
                "Encryption of email challenge failed. "
                "Please try again in a few minutes and if the problem persists, please mail nm@debian.org"))
            log.error("Caught exception while trying to encrypt email challenge.",
                      extra={'status_code': 500, 'request': self.request}, exc_info=True)
        else:
            bemail.send_nonce("notification_mails/newperson.txt",
                              self.request.user, encrypted_nonce=encrypted.decode('utf8'))
        return redirect(self.request.user.get_absolute_url())


class NewnmConfirm(VisitorMixin, View):
    """
    Confirm a pending Person object, given its nonce
    """
    def get(self, request, nonce, *args, **kw):
        if not self.request.user.is_authenticated:
            raise PermissionDenied
        if self.request.user.pending != nonce:
            raise PermissionDenied
        self.request.user.pending = ""
        self.request.user.expires = now() + datetime.timedelta(days=30)
        self.request.user.save(audit_author=self.request.user, audit_notes="confirmed pending subscription")
        return redirect(self.request.user.get_absolute_url())

from __future__ import annotations
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
from django.core import signing
from django.urls import reverse
from django import forms
from django.core.exceptions import PermissionDenied
from backend.mixins import VisitorMixin
import backend.models as bmodels
from backend import const


def is_valid_username(username):
    if username.endswith("@users.alioth.debian.org"):
        return True
    if username.endswith("@debian.org"):
        return True
    return False


def has_valid_bound_identity(person):
    for identity in person.identities.all():
        if identity.issuer == "debsso":
            if is_valid_username(identity.subject):
                return True
        elif identity.issuer == "salsa":
            return True

    return False


FORMER_ACTIVE = (const.STATUS_EMERITUS_DD, const.STATUS_REMOVED_DD)


class ClaimForm(forms.Form):
    fpr = forms.CharField(label="Fingerprint", min_length=40, widget=forms.TextInput(attrs={"size": 60}))

    def clean_fpr(self):
        data = bmodels.FingerprintField.clean_fingerprint(self.cleaned_data['fpr'])
        try:
            fpr = bmodels.Fingerprint.objects.get(fpr=self.cleaned_data["fpr"])
        except bmodels.Fingerprint.DoesNotExist:
            raise forms.ValidationError(
                    "The GPG fingerprint is not known to this system. "
                    "If you are a Debian Maintainer, and you entered the fingerprint that is in the DM keyring, "
                    "please contact Front Desk to get this fixed.")

        if not fpr.is_active:
            raise forms.ValidationError(
                    "The GPG fingerprint corresponds to a key that is not currently the active key of the user.")

        if fpr.person.status not in FORMER_ACTIVE and has_valid_bound_identity(fpr.person):
            raise forms.ValidationError(
                    "The GPG fingerprint corresponds to a person that has a valid Single Sign-On username.")

        return data


class Claim(VisitorMixin, FormView):
    """
    Validate and send an encrypted HMAC url to associate an Identity with a DM
    key
    """
    template_name = "dm/claim.html"
    form_class = ClaimForm

    def pre_dispatch(self):
        super().pre_dispatch()
        if self.request.user.is_authenticated and not self.request.user.is_superuser:
            raise PermissionDenied

        # Pick an identity allowed to claim
        self.identity = None
        for identity in self.request.signon_identities.values():
            if identity.issuer == "debsso":
                if is_valid_username(identity.subject):
                    self.identity = identity
                    break
            elif identity.issuer == "salsa":
                self.identity = identity
                break

        if self.identity is None:
            raise PermissionDenied

    def get_context_data(self, fpr=None, **kw):
        ctx = super().get_context_data(**kw)
        ctx["identity"] = self.identity
        if fpr:
            ctx["fpr"] = fpr
            ctx["person"] = fpr.person

            key = fpr.get_key()
            if not key.key_is_fresh():
                key.update_key()
            plaintext = self.request.build_absolute_uri(reverse("dm_claim_confirm", kwargs={
                "token": signing.dumps({
                    "i": self.identity.pk,
                    "f": fpr.fpr,
                })
            }))
            plaintext += "\n"
            # Add to context: it will not be rendered, but it can be picked up
            # by unit tests without the need to have access to the private key
            # to decode it
            ctx["plaintext"] = plaintext
            ctx["challenge"] = key.encrypt(plaintext.encode("utf8")).decode("utf8")
        return ctx

    def form_valid(self, form):
        fpr = bmodels.Fingerprint.objects.get(fpr=form.cleaned_data["fpr"])
        return self.render_to_response(self.get_context_data(form=form, fpr=fpr))


class ClaimConfirm(VisitorMixin, TemplateView):
    """
    Validate the claim confirmation links
    """
    template_name = "dm/claim_confirm.html"

    def pre_dispatch(self):
        super().pre_dispatch()

        # Pick an identity allowed to claim
        self.identity = None
        for identity in self.request.signon_identities.values():
            if identity.issuer == "debsso":
                if is_valid_username(identity.subject):
                    self.identity = identity
                    break
            elif identity.issuer == "salsa":
                self.identity = identity
                break

        if self.identity is None:
            raise PermissionDenied

    def validate_token(self, token):
        parsed = signing.loads(token)
        self.errors = []

        if self.request.user.is_authenticated:
            self.errors.append("Your SSO username is already associated with a person in the system")
            return False

        # Validate fingerprint
        try:
            self.fpr = bmodels.Fingerprint.objects.get(fpr=parsed["f"])
        except bmodels.Fingerprint.DoesNotExist:
            self.fpr = None
            self.errors.append("The GPG fingerprint is not known to this system")
            return False

        if not self.fpr.is_active:
            self.errors.append(
                    "The GPG fingerprint corresponds to a key that is not currently the active key of the user.")

        if self.fpr.person.status not in FORMER_ACTIVE and has_valid_bound_identity(self.fpr.person):
            self.errors.append("The GPG fingerprint corresponds to a person that has a valid Single Sign-On username.")

        if self.fpr.person.is_dd:
            self.errors.append("The GPG fingerprint corresponds to a Debian Developer.")

        # Validate username
        if self.identity.pk != parsed["i"]:
            self.errors.append("The token was not generated by you")

        if self.identity.person is not None:
            self.errors.append("The SSO username is already associated with a different person in the system")

        return not self.errors

    def get_context_data(self, **kw):
        ctx = super(ClaimConfirm, self).get_context_data(**kw)

        if self.validate_token(self.kwargs["token"]):
            # Do the mapping
            self.identity.person = self.fpr.person
            self.identity.save(audit_author=self.fpr.person, audit_notes="Claimed access via GPG claim interface")
            # self.fpr.person.save(audit_author=self.fpr.person, audit_notes="claimed account via /dm/claim")
            ctx["mapped"] = True
            ctx["person"] = self.fpr.person
            ctx["fpr"] = self.fpr
            ctx["identity"] = self.identity
        ctx["errors"] = self.errors

        return ctx

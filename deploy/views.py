from django import http
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.utils.decorators import method_decorator
from django.core.exceptions import PermissionDenied
from django.conf import settings
import os
import json
import logging

log = logging.getLogger("gitlab")


class GitlabPipeline(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kw):
        return super().dispatch(request, *args, **kw)

    def post(self, request, *args, **kw):
        gitlab_pipeline_name = getattr(settings, "DEPLOY_GITLAB_PIPELINE_NAME", "tests")
        gitlab_token = getattr(settings, "DEPLOY_GITLAB_TOKEN", None)
        if gitlab_token is not None:
            if gitlab_token != request.META.get("HTTP_X_GITLAB_TOKEN"):
                log.error("Invalid gitlab token")
                raise PermissionDenied

        log.info("gitlab submission %s", request.body.decode("utf8"))

        data = json.loads(request.body.decode("utf8"))

        if data.get("object_kind") != "pipeline":
            log.error("Wrong object_kind %s", data.get("object_kind"))
            raise PermissionDenied

        builds = data.get("builds")
        if not isinstance(builds, list):
            log.error("Wrong builds type")
            raise PermissionDenied

        builds = [b for b in builds if b.get("name") == gitlab_pipeline_name]
        if not builds:
            log.error("Found %s build not found", gitlab_pipeline_name)
            raise PermissionDenied

        if builds[0].get("status") != "success":
            log.error("Wrong build status: %s", builds[0].get("status"))
            raise PermissionDenied

        attrs = data.get("object_attributes")
        if not attrs and not isinstance(attrs, dict):
            log.error("Wrong object_attributes type: %r", attrs)
            raise PermissionDenied

        log.info("Ok to deploy %s", attrs.get("sha"))
        with open(os.path.join(settings.DEPLOY_QUEUE_DIR, "{}.deploy".format(attrs.get("sha"))), "w"):
            pass

        return http.HttpResponse("", content_type="text/plain")
